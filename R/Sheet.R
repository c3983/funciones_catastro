Sheet <- function(.x, .y, wb, startRow = 2, startCol = 1, regex_perc = "PERC", regex_num = "NUM", header_color = "darkred", font_header_color = "white"){
  style_number<-createStyle(numFmt = "#,##0")
  style_body <- createStyle(fontSize = 9, fontName = "Arial",
                            halign = "center", border= "TopBottomLeftRight", valign="center", wrapText = TRUE)
  style_head<-createStyle(fontSize = 10, fontColour = font_header_color,
                          fontName = "Arial", textDecoration = "bold", fgFill = header_color, halign="center", border= "TopBottomLeftRight")
  style_number <- createStyle(numFmt = "#,##0", halign="right")
  style_percentage <- createStyle(numFmt="PERCENTAGE")

  style_bold <- createStyle(fontSize = 10, fontColour = "black",
                            fontName = "Arial", textDecoration = "bold", border= "TopBottomLeftRight", borderStyle = "medium")


  style_none <- createStyle(border= "TopBottomLeftRight", borderStyle = "none", borderColour	= "white")

  if(.y == "Diccionario"){
    addWorksheet(wb,sheetName=.y)
    addStyle(wb, sheet = .y, style = style_none,
             rows = 1:500, cols = 1:500)
    writeData(wb,sheet=.y,x=.x, startRow = 7, startCol = 2)
    addStyle(wb, sheet = .y,style=style_body,stack=TRUE,rows = 8:(nrow(.x)+7), cols = 1:ncol(.x),gridExpand = TRUE)
    addStyle(wb, sheet = .y,style=style_head,stack=TRUE,rows = 7, cols = 1:ncol(.x),gridExpand = TRUE)
    width_vec <- apply(.x, 2, function(x) min(max(nchar(as.character(x)) + 2, na.rm = TRUE), 100, na.rm = TRUE))
    width_names<-nchar(as.character(colnames(.x)))+7
    width<-apply(cbind(width_vec,width_names),1,max)
    setColWidths(wb,  sheet = .y, cols=1:ncol(.x), widths = width)

  }else{
    if(class(try(names(wb), silent = TRUE)) != "try-error"){
      if(!(.y %in% names(wb))){
        addWorksheet(wb,sheetName=.y)
        addStyle(wb, sheet = .y, style = style_none,
                 rows = 1:500, cols = 1:500)
      }
    }else{
      addWorksheet(wb,sheetName=.y)
      addStyle(wb, sheet = .y, style = style_none,
               rows = 1:500, cols = 1:500)
    }

    writeData(wb,sheet=.y,x=.x, startRow = startRow, startCol = startCol)
    addStyle(wb, sheet = .y,style=style_body,stack=TRUE,rows = (startRow + 1):(nrow(.x) + startRow), cols = startCol:(startCol + ncol(.x) - 1), gridExpand = TRUE)
    addStyle(wb, sheet = .y,style=style_head,stack=TRUE,rows = startRow, cols = startCol:(startCol + ncol(.x) - 1),gridExpand = TRUE)
    width_vec <- apply(.x, 2, function(x) min(max(nchar(as.character(x)) + 2, na.rm = TRUE), 100, na.rm = TRUE))
    width_names<-nchar(as.character(colnames(.x)))+7
    width<-apply(cbind(width_vec,width_names),1,max)
    setColWidths(wb,  sheet = .y, cols = startCol:(startCol + ncol(.x)), widths = width)
  }


  addStyle(wb, sheet = .y,
           style=style_number,
           stack=TRUE,
           rows = (startRow + 1):(nrow(.x) + startRow),
           cols = (startCol - 1) + str_which(names(.x), regex_num),
           gridExpand = TRUE)


  addStyle(wb, sheet = .y,
           style = style_percentage,
           stack = TRUE,
           rows = (startRow + 1):(nrow(.x) + startRow),
           cols = startCol - 1 + str_which(names(.x), regex_perc),
           gridExpand = TRUE)

  if(any(str_detect(colnames(.x), "Coeficiente"))){
    #options("openxlsx.numFmt" = "0.000000000000000")
    style_number_2 <- createStyle(numFmt = "#,##0.000000000000000")
    addStyle(wb, sheet = .y,
             style = style_number_2,
             stack = TRUE,
             rows = 4:(nrow(.x)+2),
             cols = str_which(names(.x), "Coeficiente"),
             gridExpand = TRUE)
  }

}
